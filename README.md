README
This is a skeleton project for installing Node Red, Node Red Vis and all dependencies.

Installation steps:
1 Clone the repository: git clone git@bitbucket.org:nube_vis/main-project.git
2 Go to the project directory: cd main-project
3 Run the command: npm install

All neccessary modules will be installed from our repositories
